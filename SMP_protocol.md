# SMP protocol (v6)

* [SMP Procedure](#procedure)
* [Queue Subscription](#subscription)
* [Message Notification](#MessageNotification)
* [SMP transport protocol TLS](#tls)
* [SMP protocol encoding](#encoding)
    * [`serverHello`](#serverHello)
    * [`clientHello`](#clientHello)
    * [`smpTransportBlock`](#smpTransportBlock)
    * [`transmission`](#transmission)
    * [`ping`](#ping)
    * [`create`](#create)
    * [`subscribe`](#subscribe)
    * [`suspend`](#suspend)
    * [`delete`](#delete)
    * [`secure`](#secure)
    * [`acknowledge`](#acknowledge)
    * [`enableNotifications`](#enableNotifications)
    * [`disableNotifications`](#disableNotifications)
    * [`subscribeNotifications`](#subscribeNotifications)
    * [`send`](#send)
    * [`pong`](#pong)
    * [`queueIds`](#queueIds)
    * [`notifierId`](#notifierId)
    * [`ok`](#ok)
    * [`error`](#error)
    * [`unsubscribed`](#unsubscribed)
    * [`message`](#message)
    * [`messageNotification`](#messageNotification)

* [curve25519xsalsa20poly1305 encryption](#curve25519xsalsa20poly1305)

## <a id="procedure"></a>SMP Procedure

1. Alice: generate
    - a new random X25519 key pair {`rEk`, `rEkPub`} *sender*->*recipient* ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305)) encryption
    - a new random Ed448 or Ed25519 key pair {`rAuth`, `rAuthPub`} (queue *recipient* command authentication)
    - a new random X25519 key pair {`rDh`, `rDhPub`} *server*->*recipient* ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305)) encryption of [`rcvMsgBody`](#message)
2. Alice: establish connection to SMP server, [`TLS`](#tls) Handshake
3. Server: [`serverHello`](#serverHello)
4. Alice: [`clientHello`](#clientHello)
5. Alice: [`create`](#create) queue request signed with `rAuth`, contains `rAuthPub` and `rDhPub`
6. Server:
    - generate a new random X25519 key pair {`srvRDh`, `srvRDhPub`} *server*->*recipient* ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305)) encryption of [`rcvMsgBody`](#message)
    - compute *server*->*recipient* Shared Secret `srvSS` from `srvRDh` and `rDhPub` ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305))
7. Server: [`queueIds`](#queueIds) response, contains
    - `recipientId`, queueId used by Alice
    - `senderId`, queueId to be given to Bob in the Out of Band message
    - `srvRDhPub`
8. Alice: compute *server*->*recipient* Shared Secret `srvSS` from `rDh` and `srvRDhPub` ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305))
9. Alice: send *Out of Band* message to Bob that contains:
    - SMP server address and `serverIdentity`
    - `senderId`
    - `rEkPub`
10. Bob:
    - generate a new random X25519 key pair {`sEk`, `sEkPub`} *sender*->*recipient* ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305)) encryption
    - generate a new random Ed448 or Ed25519 key pair {`sAuth`, `sAuthPub`} (queue *sender* command authentication)
    - compute *sender*->*recipient* Shared Secret `rsSS` from `sEk` and `rEkPub` ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305))
11. Bob: establish connection to SMP server, [`TLS`](#tls) Handshake
12. Server: [`serverHello`](#serverHello)
13. Bob: [`clientHello`](#clientHello)
14. Bob: include `sAuth` in `sentMsgBody`, encrypt `sentMsgBody` with `rsSS`
15. Bob: request [`send`](#send) "CONFIRM" message, contains `sentMsgBody` and `sEkPub`
16. Server: response [`ok`](#ok)
17. Server: indicate [`message`](#message) to Alice
18. Alice: decrypt *server*->*recipient* with `srvSS`, obtain `sEkPub` ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305))
19. Alice: compute *sender*->*recipient* Shared Secret `rsSS` from `sEkPub` and `rEk` ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305))
20. Alice: decrypt `sentMsgBody` with `rsSS`, obtain `sAuth` ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305))
21. Alice: request [`acknowledge`](#acknowledge) of message reception
22. Server: response [`ok`](#ok)
23. Alice: request [`secure`](#secure) of queue with `sAuthPub`
24. Server: response [`ok`](#ok)


```mermaid
sequenceDiagram
autonumber
participant Bob as Bob (sender)
participant Server as SMP Server
participant Alice as Alice (recipient)
Alice-->Alice: generate rEk, rAuth and rDh
Alice-->Server: TLS Handshake
Server->>Alice : serverHello
Alice->>Server : clientHello
Alice->>Server: NEW
Server-->Server: generate srvRDh, compute srvSS
Server->>Alice: IDS
Alice-->Alice: compute srvSS
Alice-->>Bob: Out of Band (senderId, rEkPub)
Bob-->Bob: generate sEk and sAuth, compute rsSS
Bob-->Server: TLS Handshake
Server->>Bob: serverHello
Bob->>Server: clientHello
Bob-->Bob: encrypt with rsSS
Bob->>Server: SEND
Server->>Bob: OK
Server->>Alice: MSG
Alice-->Alice: decrypt with srvSS
Alice-->Alice: compute rsSS
Alice-->Alice: decrypt with rsSS
Alice->>Server: ACK
Server->>Alice: OK
Alice->>Server: KEY
Server->>Alice: OK
```

## <a id="subscription"></a>Queue Subscription

1. Alice: request [`subscribe`](#subscribe) to a queue `rId`
2. Server: response [`ok`](#ok)
3. Alice (second connection): request [`subscribe`](#subscribe) to the same queue `rId`
4. Server: indicates [`unsubscribed`](#unsubscribed) to Alice (first connection)
5. Server: response [`ok`](#ok) to Alice (second connection)


```mermaid
sequenceDiagram
autonumber
participant Alice0 as Alice
participant Server as SMP Server
participant Alice1 as Alice (second connection)
Alice0->>Server: SUB
Server->>Alice0: OK
opt second transport connection subscribes to the same queue
Alice1->>Server: SUB
Server->>Alice0: END
Server->>Alice1: OK
end
```

## <a id="MessageNotification"></a>Message Notification

Variant of [SMP Procedure](#procedure) with a *notifier* client.

1. Alice: generate
    - a new random X25519 key pair {`rEk`, `rEkPub`} *sender*->*recipient* ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305)) encryption
    - a new random Ed448 or Ed25519 key pair {`rAuth`, `rAuthPub`} (queue *recipient* command authentication)
    - a new random Ed448 or Ed25519 key pair {`nAuth`, `nAuthPub`} (queue *notifier* command authentication)
    - a new random X25519 key pair {`rDh`, `rDhPub`} *server*->*recipient* ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305)) encryption of [`rcvMsgBody`](#message)
    - a new random X25519 key pair {`nEk`, `nEkPub`} *server*->*recipient* ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305)) encryption of [`NMsgMeta`](#messageNotification)
2. Alice: establish connection to SMP server, [`TLS`](#tls) Handshake
3. Server: [`serverHello`](#serverHello)
4. Alice: [`clientHello`](#clientHello)
5. Alice: [`create`](#create) queue request signed with `rAuth`, contains `rAuthPub` and `rDhPub`
6. Server:
    - generate a new random X25519 key pair {`srvRDh`, `srvRDhPub`} *server*->*recipient* ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305)) encryption of [`rcvMsgBody`](#message)
    - compute *server*->*recipient* Shared Secret `srvSS` from `srvRDh` and `rDhPub` ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305))
7. Server: [`queueIds`](#queueIds) response, contains
    - `recipientId`, queueId used by Alice
    - `senderId`, queueId to be given to Bob in the Out of Band message
    - `srvRDhPub`
8. Alice: compute *server*->*recipient* Shared Secret `srvSS` from `rDh` and `srvRDhPub` ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305))
9. Alice: [`enableNotifications`](#enableNotifications) request signed with `rAuth`, contains `nAuthPub` and `nEkPub`
10. Server: generate a new random X25519 key pair {`srvNDh`, `srvNDhPub`} *server*->*recipient* ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305)) encryption of [`NMsgMeta`](#messageNotification)
11. Server: [`notifierId`](#notifierId) response, contains
    - `notifierId`, queueId to be given to Notifier in Out of Band message
    - `srvNDhPub`
12. Alice: compute *server*->*recipient* Shared Secret `nSS` from `nEk` and `srvNDhPub` ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305))
13. Alice: send *Out of Band* message to Notifier that contains `notifierId` and `nAuth`; **Alice disconnects the TLS transport**
14. Notifier: establish connection to SMP server, [`TLS`](#tls) Handshake
15. Server: [`serverHello`](#serverHello)
16. Notifier: [`clientHello`](#clientHello)
17. Notifier: [`subscribeNotifications`](#subscribeNotifications) request signed with `nAuth`
18. Server: response [`ok`](#ok)
19. Alice: send *Out of Band* message to Bob that contains:
    - SMP server address and `serverIdentity`
    - `senderId`
    - `rEkPub`
20. Bob:
    - generate a new random X25519 key pair {`sEk`, `sEkPub`} *sender*->*recipient* ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305)) encryption
    - generate a new random Ed448 or Ed25519 key pair {`sAuth`, `sAuthPub`} (queue *sender* command authentication)
    - compute *sender*->*recipient* Shared Secret `rsSS` from `sEk` and `rEkPub` ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305))
21. Bob: establish connection to SMP server, [`TLS`](#tls) Handshake
22. Server: [`serverHello`](#serverHello)
23. Bob: [`clientHello`](#clientHello)
24. Bob: include `sAuth` in `sentMsgBody`, encrypt `sentMsgBody` with `rsSS`
25. Bob: request [`send`](#send) "CONFIRM" message, contains `sentMsgBody` and `sEkPub`; `msgFlag` is **notify**.
26. Server: response [`ok`](#ok)
27. Server: indicate [`messageNotification`](#messageNotification) to Notifier
28. Notifier: send *Out of Band* message to Alice that contains: `nonce` and `encryptedNMsgMeta`
29. Alice: decrypt `encryptedNMsgMeta` with `nSS`, obtain `msgId` and `timeStamp` ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305))
30. Alice: establish connection to SMP server, [`TLS`](#tls) Handshake
31. Server: [`serverHello`](#serverHello)
32. Alice: [`clientHello`](#clientHello)
33. Alice: request [`subscribe`](#subscribe) to `recipientId` queue
34. Server: indicate [`message`](#message) to Alice
35. Alice: decrypt *server*->*recipient* with `srvSS`, obtain `sEkPub` ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305))
36. Alice: compute *sender*->*recipient* Shared Secret `rsSS` from `sEk` and `rEkPub` ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305))
37. Alice: decrypt `sentMsgBody` with `rsSS`, obtain `sAuthPub` ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305))
38. Alice: request [`acknowledge`](#acknowledge) of message reception
39. Server: response [`ok`](#ok)
40. Alice: request [`secure`](#secure) of queue with `sAuthPub`
41. Server: response [`ok`](#ok)


```mermaid
sequenceDiagram
autonumber
participant Alice as Alice (recipient)
participant Server as SMP Server
Alice-->Alice: generate rEk, nEk, rAuth, nAuth and rDh
Alice-->Server: TLS Handshake
Server->>Alice : serverHello
Alice->>Server : clientHello
Alice->>Server: NEW
Server-->Server: generate srvRDh, compute srvSS
Server->>Alice: IDS
Alice-->Alice: compute srvSS
Alice->>Server: NKEY
Server-->Server: generate srvNDh, compute nSS
Server->>Alice: NID
Alice-->Alice: compute nSS
create participant Notifier
Alice-->>Notifier: Out of Band (notifierId, nAuth)
Note over Alice,Server: disconnect TLS transport
Notifier-->Server: TLS Handshake
Server->>Notifier: serverHello
Notifier->>Server: clientHello
Notifier->>Server: NSUB
Server->>Notifier: OK
create participant Bob as Bob (sender)
Alice-->>Bob: Out of Band (senderId, rEkPub)
Bob-->Bob: generate sEk and sAuth, compute rsSS
Bob-->Server: TLS Handshake
Server->>Bob: serverHello
Bob->>Server: clientHello
Bob-->Bob: encrypt with rsSS
Bob->>Server: SEND
Server->>Bob: OK
Server->>Notifier: NMSG
Notifier-->>Alice: Out of Band (nonce, encryptedNMsgMeta)
Alice-->Alice: decrypt with nSS
Alice-->Server: TLS Handshake
Server->>Alice: serverHello
Alice->>Server: clientHello
Alice->>Server: SUB
Server->>Alice: MSG
Alice-->Alice: decrypt with srvSS
Alice-->Alice: compute rsSS
Alice-->Alice: decrypt with rsSS
Alice->>Server: ACK
Server->>Alice: OK
Alice->>Server: KEY
Server->>Alice: OK
```


## <a id="tls"></a>SMP transport protocol TLS

TLS 1.3 with
 * TLS_CHACHA20_POLY1305_SHA256 cipher suite
 * Ed25519 or Ed448 EdDSA for signatures
 * X25519 or X448 ECDHE for key exchange

 TLS server and client must not allow session resumption

 The server sends a certificate chain of exactly two certificates in the TLS handshake:

 * a self-signed *Offline Certificate*
 * the *Server Certificate* signed by the *Offline Certificate*

 The client must abort the handshake if the server does not send exactly two certificates.

 The client must abort the handshake if the fingerprint of the *Offline Certificate* (SHA256 hash of the DER encoded X.509 cert) does not equal the `serverIdentity` (as received as part of the SMP server address).

After successful TLS handshake, client and server exchange blocks of fixed size (16384 bytes).

The first block sent by the server is [`serverHello`](#serverHello) to announce the supported SMP version range and a `sessionIdentifier`.

The first block sent by the client is [`clientHello`](#clientHello) with its SMP version and the `serverIdentity`.

All further traffic consists of [`smpTransportBlock`](#smpTransportBlock)s.

## <a id="encoding"></a>SMP protocol encoding

----

### <a id="serverHello"></a>`serverHello`


| 2 | 2 | 2 | 1 | `sL`| 16384 - `sL` - 7 |
|---|---|---|---|---|---|
|`oL`|`minSmpVersion`|`maxSmpVersion`|`sL`| `sessionIdentifier` |`pad`|
|`0x00`, `0x25`|`0x00`, `0x01`|`0x00`, `0x06`|`0x20`|`0x??`, ..., `0x??`|`0x23`, ..., `0x23`|

* first block sent by the server
* `oL` (16 bit unsigned integer in network byte order) : 5 + `sL` = 37
* `minSmpVersion` (16 bit unsigned integer in network byte order) : 1
* `maxSmpVersion` (16 bit unsigned integer in network byte order) : 6
* `sL` (8 bit unsigned integer) : 32
* `sessionIdentifier` : 32 bytes
* `pad` : 16345 byte padding {0x23, ..., 0x23}

----

### <a id="clientHello"></a>`clientHello`

| 2 | 2 | 1 | `sL` | 16384 - `sL` - 5 |
|---|---|---|---|---|
|`oL`|`smpVersion`|`sL`|`serverIdentity`|`pad`|
|`0x00`, `0x23`|`0x00`, `0x06`|`0x20`|`0x??`, ..., `0x??`|`0x23`, ..., `0x23`|

* first block sent by the client
* `oL` (16 bit unsigned integer in network byte order) : 3 + `sL` = 35
* `smpVersion` (16 bit unsigned integer in network byte order) : 6
* `sL` (8 bit unsigned integer) : 32
* `serverIdentity` : 32 byte SHA256 hash of the DER encoded *Offline Certificate*
* `pad` : 16347 byte padding {0x23, ..., 0x23}

----

### <a id="smpTransportBlock"></a>`smpTransportBlock`

| 2 | 1 |N| 16384 - N - 3|
|---|---|---|---|
|`oL`|`transmissionCount`|`transmission`s|`pad`|

* `oL` (16 bit unsigned integer in network byte order) : N + 1
* `transmissionCount` (8 bit unsigned integer > 0): number of [`transmission`](#transmission)s
* `transmission`s : N bytes of `transmissionCount` concatenated [`transmission`](#transmission)s
* `pad` : 16384 - N - 3 byte padding {0x23, ..., 0x23}

----

### <a id="transmission"></a>`transmission`

| 2 |1|`sigL`|1|`sessL`|1|`cL`|1|`qL`|N|
|---|---|---|---|---|---|---|---|---|---|
|`tL`|`sigL`|`signature`|`sessL`|`sessionIdentifier`|`cL`|`corrId`|`qL`|`queueId`|`smpCommand`|

* `tL` (16 bit unsigned integer in network byte order) : N + 4 + `sigL` + `sessL` + `cL` + `qL`
* `sigL` (8 bit unsigned integer) : length of `signature`, might be 0
* `signature` : `sigL` bytes Ed25519 or Ed448 signature over (`sessL`, `sessionIdentifier`, `cL`, `corrId`, `qL`, `queueId`, `smpCommand`) used to authenticate (some) requests
* `sessL` : (8 bit unsigned integer) : length of `sessionIdentifier`
* `sessionIdentifier` : `sessL` bytes session identifier as received in [`serverHello`](#serverHello)
* `cL` (8 bit unsigned integer) : length of `corrId`, might be 0
* `corrId` : `cL` bytes correlation identifier use to correlate requests/response
* `qL` (8 bit unsigned integer) : length of `queueId`, might be 0
* `queueId` : `qL` bytes queue identifier
* `smpCommand` : N bytes, one of
    - generic client request
        - [`ping`](#ping)
    - *recipient* client requests, one of
        - [`create`](#create)
        - [`subscribe`](#subscribe)
        - [`suspend`](#suspend)
        - [`delete`](#delete)
        - [`secure`](#secure)
        - [`acknowledge`](#acknowledge)
        - [`enableNotifications`](#enableNotifications)
        - [`disableNotifications`](#disableNotifications)
    - *notifier* client request
        - [`subscribeNotifications`](#subscribeNotifications)
    - *sender* client request
        - [`send`](#send)
    - server response, one of
        - [`pong`](#pong)
        - [`queueIds`](#queueIds)
        - [`notifierId`](#notifierId)
        - [`ok`](#ok)
        - [`error`](#error)
    - server indication, one of
        - [`unsubscribed`](#unsubscribed)
        - [`message`](#message)
        - [`messageNotification`](#messageNotification)

----

### <a id="ping"></a>`ping` client request

|4|
|---|
|`"PING"`|

* request sent by client
* `signature` and `queueId` of the enclosing [`transmission`](#transmission) are empty
* `corrId` of the enclosing [`transmission`](#transmission) is set
* server will respond with [`pong`](#pong)

----

### <a id="create"></a>`create` *recipient* client request

| 4 | 1 | `rAuthPubL` | 1 | `rDhPubL`|1|
|---|---|---|---|---|---|
|`"NEW"`, `0x20`|`rAuthPubL`|`rAuthPub`|`rDhPubL`|`rDhPub`|`createOnlyOrSubscribe`|

* request sent by *recipient* client
* `queueId` of the enclosing [`transmission`](#transmission) is empty
* `corrId` of the enclosing [`transmission`](#transmission) is set
* `signature` of the enclosing [`transmission`](#transmission) is the Ed448 or Ed25519 signature of (`sessL`, `sessionIdentifier`, `cL`, `corrId`, `qL`, `queueId`, `smpCommand`) with `rAuth`
* `rAuthPubL` (8 bit unsigned integer) : length of `rAuthPub`
* `rAuthPub` : X.509 encoded public part of the Ed448 or Ed25519 key that will be used to authenticate all client requests related to the new queue
* `rDhPubL` (8 bit unsigned integer) : length of `rDhPub`
* `rDhPub` : X.509 encoded public part of the X25519 key used in *server*->*recipient* ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305)) encryption of [`rcvMsgBody`](#message)
* `createOnlyOrSubscribe` is
   - `"C"` : for queue creation
   - `"S"` : for queue creation and subscription
* server will respond with [`queueIds`](#queueIds) or [`error`](#error)

----

### <a id="subscribe"></a>`subscribe` *recipient* client request

|3|
|---|
|`"SUB"`|

* request sent by *recipient* client
* `queueId` of the enclosing [`transmission`](#transmission) is the *recipient* queue identifier to subscribe to
* `corrId` of the enclosing [`transmission`](#transmission) is set
* `signature` of the enclosing [`transmission`](#transmission) is the Ed448 or Ed25519 signature of (`sessL`, `sessionIdentifier`, `cL`, `corrId`, `qL`, `queueId`, `smpCommand`) with `rAuth`
* server will respond with [`ok`](#ok) or [`error`](#error) or the first available [`message`](#message)

----

### <a id="suspend"></a>`suspend` *recipient* client request

|3|
|---|
|`"OFF"`|

* request sent by *recipient* client
* `queueId` of the enclosing [`transmission`](#transmission) is the *recipient* queue to suspend
* `corrId` of the enclosing [`transmission`](#transmission) is set
* `signature` of the enclosing [`transmission`](#transmission) is the Ed448 or Ed25519 signature of (`sessL`, `sessionIdentifier`, `cL`, `corrId`, `qL`, `queueId`, `smpCommand`) with `rAuth`
* server will respond with [`ok`](#ok) or [`error`](#error)

----

### <a id="delete"></a>`delete` *recipient* client request

|3|
|---|
|`"DEL"`|

* request sent by *recipient* client
* `queueId` of the enclosing [`transmission`](#transmission) is the *recipient* queue to delete
* `corrId` of the enclosing [`transmission`](#transmission) is set
* `signature` of the enclosing [`transmission`](#transmission) is the Ed448 or Ed25519 signature of (`sessL`, `sessionIdentifier`, `cL`, `corrId`, `qL`, `queueId`, `smpCommand`) with `rAuth`
* server will respond with [`ok`](#ok) or [`error`](#error)

----

### <a id="secure"></a>`secure` *recipient* client request

|4|1|`sAuthPubL`|
|---|---|---|
|`"KEY"`, `0x20`|`sAuthPubL`|`sAuthPub`|

* request sent by *recipient* client
* `queueId` of the enclosing [`transmission`](#transmission) is the *recipient* queue to secure
* `corrId` of the enclosing [`transmission`](#transmission) is set
* `signature` of the enclosing [`transmission`](#transmission) is the Ed448 or Ed25519 signature of (`sessL`, `sessionIdentifier`, `cL`, `corrId`, `qL`, `queueId`, `smpCommand`) with `rAuth`
* `sAuthPubL` (8 bit unsigned integer) : length of `sAuthPub`
* `sAuthPub` : `sAuthPubL` bytes X.509 encoded public part of the Ed448 or Ed25519 key the *sender* must use to authenticate [`send`](#send) [`transmission`](#transmission)s to (the `senderId` of) this queue from now on
* server will respond with [`ok`](#ok) or [`error`](#error)

----

### <a id="acknowledge"></a>`acknowledge` *recipient* client request

|3|1|`mIdL`|
|---|---|---|
|`"ACK"`|`mIdL`|`messageId`|

* request sent by *recipient* client
* `queueId` of the enclosing [`transmission`](#transmission) is the *recipient* queue for which the reception of a message is acknowledged
* `corrId` of the enclosing [`transmission`](#transmission) is set
* `signature` of the enclosing [`transmission`](#transmission) is the Ed448 or Ed25519 signature of (`sessL`, `sessionIdentifier`, `cL`, `corrId`, `qL`, `queueId`, `smpCommand`) with `rAuth`
* `mIdL` (8 bit unsigned integer) : length of `messageId`
* `messageId` : `mIdL` bytes message identifier
* server will respond with [`ok`](#ok) or [`error`](#error)

----

### <a id="enableNotifications"></a>`enableNotifications` *recipient* client request

|5|1|`nAuthPubL`|1|`nEkPubL`|
|---|---|---|---|---|
|`"NKEY"`, `0x20`|`nAuthPubL`|`nAuthPub`|`nEkPubL`|`nEkPub`|

* request sent by *recipient* client
* `queueId` of the enclosing [`transmission`](#transmission) is the *sender* queue for which notifications will be enabled
* `corrId` of the enclosing [`transmission`](#transmission) is set
* `signature` of the enclosing [`transmission`](#transmission) is the Ed448 or Ed25519 signature of (`sessL`, `sessionIdentifier`, `cL`, `corrId`, `qL`, `queueId`, `smpCommand`) with `rAuth`
* `nAuthPubL` (8 bit unsigned integer) : length of `nAuthPub`
* `nAuthPub` : `nAuthPubL` bytes X.509 encoded public part of the Ed448 or Ed25519 key the *notifier* must use to authenticate [`subscribeNotifications`](#subscribeNotifications) requests
* `nEkPubL` (8 bit unsigned integer) : length of `nEkPub`
* `nEkPub` : `nEkPubL` bytes X.509 encoded public part of the X25519 key used in *server*->*recipient* ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305)) encryption of [`NMsgMeta`](#messageNotification)
* server will respond with [`notifierId`](#notifierId) or [`error`](#error)

----

### <a id="disableNotifications"></a>`disableNotifications` *recipient* client request

|4|
|---|
|`"NDEL"`|

* request sent by *recipient* client
* `queueId` of the enclosing [`transmission`](#transmission) is the *sender* queue for which notifications will be disabled
* `corrId` of the enclosing [`transmission`](#transmission) is set
* `signature` of the enclosing [`transmission`](#transmission) is the Ed448 or Ed25519 signature of (`sessL`, `sessionIdentifier`, `cL`, `corrId`, `qL`, `queueId`, `smpCommand`) with `rAuth`
* server will respond with [`ok`](#ok) or [`error`](#error)

----

### <a id="subscribeNotifications"></a>`subscribeNotifications` *notifier* client request

|4|
|---|
|`"NSUB"`|

* request sent by *notifier* client
* `queueId` of the enclosing [`transmission`](#transmission) is the *notifier* queue to subscribe to
* `corrId` of the enclosing [`transmission`](#transmission) is set
* `signature` of the enclosing [`transmission`](#transmission) is the Ed448 or Ed25519 signature of (`sessL`, `sessionIdentifier`, `cL`, `corrId`, `qL`, `queueId`, `smpCommand`) with `nAuth`
* server will respond with [`ok`](#ok) or [`error`](#error)

----

### <a id="send"></a>`send` *sender* client request

|5|1|1|2|1|0..16085|
|---|---|---|---|---|---|
|`"SEND"`, `0x20`|`msgFlag`|`0x20`|`smpClientVersion`|`0x00`|`sentMsgBody`|

**OR** (if this is a "CONFIRM" message)

|5|1|1|2|1|1|`sEkPubL`|0..16040|
|---|---|---|---|---|---|---|---|
|`"SEND"`, `0x20`|`msgFlag`|`0x20`|`smpClientVersion`|`0x01`|`sEkPubL`|`sEkPub`|`sentMsgBody`|

* request sent by *sender* client
* `queueId` of the enclosing [`transmission`](#transmission) is the *sender* queue to which the message is intended
* `corrId` of the enclosing [`transmission`](#transmission) is set
* `signature` of the enclosing [`transmission`](#transmission) is one of
    * the Ed448 or Ed25519 signature of (`sessL`, `sessionIdentifier`, `cL`, `corrId`, `qL`, `queueId`, `smpCommand`) with `sAuth` key, if the queue was secured by the *recipient*
    * empty: if the queue was not yet secured by the recipient
* `msgFlag` one of
     - `"T"` : notify
     - `"F"` : do not notify
* `smpClientVersion` (16 bit unsigned integer in network byte order) : 2
* `sEkPubL` (8 bit unsigned integer): length of `sEkPub`
* `sEkPub`: `sEkPubL` bytes X.509 encoded public part of the X25519 key used in *sender*->*recipient* ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305)) encryption of `sentMsgBody`
* `sentMsgBody` : *sender*->*recipient* ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305)) encrypted Data
* server will respond with [`ok`](#ok) or [`error`](#error)

----

### <a id="pong"></a>`pong` *server* response

|4|
|---|
|`"PONG"`|

* *server* response to client [`ping`](#ping) request
* `signature` and `queueId` of the enclosing [`transmission`](#transmission) are empty
* `corrId` of the enclosing [`transmission`](#transmission) matches the `corrId` in the [`ping`](#ping) request

----

### <a id="queueIds"></a>`queueIds` *server* response

| 4 | 1 | `rIdL` | 1 | `sIdL` | 1 | `srvRDhPubL`|
|---|---|---|---|---|---|---|
|`"IDS"`, `0x20`|`rIdL` |`recipientId`|`sIdL`|`senderId`|`srvRDhPubL`|`srvRDhPub`|

* *server* response to *recipient* client [`create`](#create) request
* `signature` and `queueId` of the enclosing [`transmission`](#transmission) are empty
* `corrId` of the enclosing [`transmission`](#transmission) matches the `corrId` in the [`create`](#create) request
* `rIdL` (8 bit unsigned integer) : length of `recipientId`
* `recipientId` : `rIdL` bytes, identifies *recipient* end of the queue
* `sIdL` (8 bit unsigned integer) : length of `senderId`
* `senderId` : `sIdL` bytes, identifies the *sender* end of the queue
* `srvRDhPubL` (8 bit unsigned integer) : length of `srvRDhPub`
* `srvRDhPub` : `srvRDhPubL` bytes X.509 encoded public part of the X25519 key used in *server*->*recipient* ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305)) encryption of [`rcvMsgBody`](#message)

----

### <a id="notifierId"></a>`notifierId` *server* response

|4|1|`nIdL`|1|`srvNDhL`|
|---|---|---|---|---|
|`"NID"`, `0x20`|`nIdL`|`notifierId`|`srvNDhPubL`|`srvNDhPub`|

* *server* response to *recipient* client [`enableNotifications`](#enableNotifications) request
* `signature` and `queueId` of the enclosing [`transmission`](#transmission) are empty
* `corrId` of the enclosing [`transmission`](#transmission) matches the `corrId` in the [`enableNotifications`](#enableNotifications) request
* `nIdL` (8 bit unsigned integer) : length of `notifierId`
* `notifierId` : `nIdL` bytes, identifies the *notifier* queue
* `srvNDhPubL` (8 bit unsigned integer) : length of `srvRDhPub`
* `srvNDhPub` : `srvNDhPubL` bytes X.509 encoded public part of the X25519 key used in *server*->*recipient* ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305)) encryption of [`NMsgMeta`](#messageNotification)

----

### <a id="ok"></a>`ok` *server* response

|2|
|---|
|`"OK"`|

* *server* response to *sender* or *recipient* client request
* `signature` of the enclosing [`transmission`](#transmission) is empty
* `corrId` of the enclosing [`transmission`](#transmission) matches the `corrId` in the request
* `queueId` of the enclosing [`transmission`](#transmission) matches the `queueId` in the request

----

### <a id="error"></a>`error` *server* response

|N|
|---|
|`errorString`|

* *server* response to *sender* or *recipient* client request
* `signature` of the enclosing [`transmission`](#transmission) is empty
* `corrId` of the enclosing [`transmission`](#transmission) matches the `corrId` in the request
* `queueId` of the enclosing [`transmission`](#transmission) matches the `queueId` in the request or is empty
* `errorString` is one of
    - `"ERR BLOCK"`
    - `"ERR SESSION"`
    - `"ERR AUTH"`
    - `"ERR LARGE_MSG"`
    - `"ERR INTERNAL"`
    - `"ERR CMD SYNTAX"`
    - `"ERR CMD PROHIBITED"`
    - `"ERR CMD NO_AUTH"`
    - `"ERR CMD HAS_AUTH"`
    - `"ERR CMD NO_ENTITY"`

----

### <a id="unsubscribed"></a>`unsubscribed` *server* indication

|3|
|---|
|`"END"`|

* indication sent by *server* to *recipient* or *notifier* client
* `queueId` of the enclosing [`transmission`](#transmission) is the *recipient* or *notifier* queue for which the subscription end is indicated
* `corrId` of the enclosing [`transmission`](#transmission) is empty
* `signature` of the enclosing [`transmission`](#transmission) is empty

----

### <a id="message"></a>`message` *server* indication

|4|1|`mIdL`|16122|
|---|---|---|---|
|`"MSG"`, `0x20`|`mIdL`|`messageId`|`encryptedRcvMsgBody`|

* indication sent by *server* to *recipient* client
* `queueId` of the enclosing [`transmission`](#transmission) is the *recipient* queue
* `corrId` and `signature` of the enclosing [`transmission`](#transmission) are empty
* `mIdL` (8 bit unsigned integer) : length of `messageId`
* `messageId` : `mIdL` bytes message identifier used as **Nonce** in the *server*->*recipient* ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305)) encryption of `rcvMsgBody`
* `encryptedRcvMsgBody` : 16122 bytes *server*->*recipient* ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305)) encrypted `rcvMsgBody`

where `rcvMsgBody` is 16122 - 16 = 16106

|2|8|1|1|2|1|N|16106 - 15 - N|
|---|---|---|---|---|---|---|---|
|`oL`|`timeStamp`|`msgFlag`|`0x20`|`smpClientVersion`|`0x00`|`sentMsgBody`|`pad`|

**OR** (if this is a "CONFIRM" message)

|2|8|1|1|2|1|1|`sEkPubL`|N|16106 - 16 - N - `sEkPubL`|
|---|---|---|---|---|---|---|---|---|---|
|`oL`|`timeStamp`|`msgFlag`|`0x20`|`smpClientVersion`|`0x01`|`sEkPubL`|`sEkPub`|`sentMsgBody`|`pad`|

* `oL` (16 bit unsigned integer in network byte order) : N + 13 (or N + 14 + `sEkPubL`)
* `timeStamp` (64 bit unsigned integer in network byte order): Unix time
* `msgFlag` one of
     - `"T"` : notify
     - `"F"` : do not notify
* `smpClientVersion` : 2
* `sEkPubL` (8 bit unsigned integer) : length of `sEkPub`
* `sEkPub` : `sEkPubL` byte X.509 encoded public key part of the X25519 key used in *sender*->*recipient* ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305)) encryption
* `sentMsgBody` : N bytes *sender*->*recipient* ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305)) encrypted Data
* `pad` : 16106 - 16 - N - `sEkPubL` bytes padding {0x23, ..., 0x23}

----

### <a id="messageNotification"></a>`messageNotification` *server* indication

|5|24|1|`eL`|
|---|---|---|---|
|`"NMSG"`, `0x20`|`nonce`|`eL`|`encryptedNMsgMeta`|

* indication sent by *server* to *notifier* client
* `queueId` of the enclosing [`transmission`](#transmission) is the *notifier* queue
* `corrId` and `signature` of the enclosing [`transmission`](#transmission) are empty
* `nonce` is the 24 byte **Nonce* used in the *server*->*recipient* ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305)) encryption of `NMsgMeta`
* `eL` (8 bit unsigned integer) : length of `encryptedNMsgMeta`
* `encryptedNMsgMeta` : `eL` bytes *server*->*recipient* ([curve25519xsalsa20poly1305](#curve25519xsalsa20poly1305)) encrypted `NMsgMeta`

where `NMsgMeta` is `eL` - 16 

|2|1|`mIdL`|8| `eL` - 16 - 11 - `mIdL`|
|---|---|---|---|---|
|`oL`|`mIdL`|`messageId`|`timeStamp`|`pad`|

* `oL` (16 bit unsigned integer in network byte order) : 8 + `mIdL`
* `mIdL` (8 bit unsigned integer) : length of `messageId`
* `messageId` : `mIdL` bytes message identifier
* `timeStamp` (64 bit unsigned integer in network byte order): Unix time
* `pad` : `eL` - 16 - 11 - `mIdL` bytes padding {0x23, ..., 0x23}

----

## <a id="curve25519xsalsa20poly1305"></a> curve25519xsalsa20poly1305 encryption

The 32 byte SharedSecret is computed from X25519 Keys as defined in [RFC7748](https://www.rfc-editor.org/rfc/rfc7748).

A [NaCl](https://cr.yp.to/highspeed/naclcrypto-20090310.pdf) like (but incompatible) crypto_box is used.

The implementation of the crypto_box is documented in the (MIT Licensed) [Haskell Crypton Tutorial.hs](https://hackage.haskell.org/package/crypton-0.34/docs/Crypto-Tutorial.html)